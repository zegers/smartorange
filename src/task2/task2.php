<?php


require_once "Matrix.php";


$matrixSize = array('width' => 10, 'height' => 10);


$matrix = new Matrix($matrixSize);

$matrix->makeRandomPointTrue();

$steps = $matrix->fillMatrix();


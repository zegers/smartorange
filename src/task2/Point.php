<?php

/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 15.07.2016
 * Time: 16:57
 */
class Point
{
    public $x;
    public $y;
    public $value = 0;
    public $flag;

    public function __construct($y, $x, $value = 0)
    {
        $this->x =  $x;
        $this->y =  $y;
        $this->value =  $value;
        $this->flag = false;
    }

    public function setValue($val = 1) {
        $this->value = $val;
    }
    public function isZero() {
        return ($this->value === 0)? true: false;
    }
}
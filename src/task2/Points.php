<?php

/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 15.07.2016
 * Time: 17:00
 */
class Points
{

    public $points;
    public function __construct()
    {
        $this->points = array();
    }


    public function push($point) {
        $this->points[] = $point;
    }
    public function setPointValueByHeightWidth($newY, $newX, $val) {
        $point = $this->getPointByHeightWidth($newY, $newX);
        $point->setValue(1);
       
    }
    public function setPointFlagByHeightWidth($newY, $newX, $val) {
        $point = $this->getPointByHeightWidth($newY, $newX);
        $point->flag = true;

    }

    public function getPointByHeightWidth($height, $width) {
        foreach ($this->points as $point) {
            if ($point->x == $width AND $point->y == $height) {
                 return $point;
            }
        }
    }

    public function getPointByNumber($number) {

        if (  $number > count($this->points)) {
            throw new Exception("Cant get point by number!!!");
        }

        return $this->points[$number];
    }
}
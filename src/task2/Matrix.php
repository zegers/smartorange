<?php

/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 15.07.2016
 * Time: 17:02
 */

require_once "Point.php";
require_once "Points.php";

class Matrix
{

    public $points;
    public $startPoint;

    public $helpArray = array(-1, 0, 1);
    public $sizeHelpArray;

    public $width;
    public $height;

    public $steps;
    public $sum;

    public function __construct($size = array(10,10))
    {
        $this->width = $size['width'];
        $this->height = $size['height'];
        $this->sum = $this->width * $this->height;
        $this->points = new Points();
        
        $this->sizeHelpArray = count($this->helpArray);
        
        for ($i = 0; $i < $this->height; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $point = new Point($i, $j, 0);

                $this->points->push($point);
            }
        }
    }

    public function isEqualSum($result) {
        if ( $result !== $this->sum)
            return false;
        return true;
    }

    public function makeRandomPointTrue() {
        $randNumber = rand(0, count($this->points->points)-1);
        
        $this->startPoint = $this->points->getPointByNumber($randNumber);
        

        if( ! $this->startPoint->isZero() ) {
            throw new Exception("makeRandomPointTrue : point not a Zero");
        }

        $this->startPoint->setValue(1);
        
    }


    public function fillPointsAroundPointbyNumber($point){

        for ($y = 0; $y < $this->sizeHelpArray; $y++ ) {
            for ($x = 0; $x < $this->sizeHelpArray; $x++ ) {
                $newX = $point->x+$this->helpArray[$x];
                $newY = $point->y+$this->helpArray[$y];
//                echo "$newX , $newY</br>";

                $newPoint = $this->points->getPointByHeightWidth($newY, $newX);

//                var_dump($newPoint);
//                echo "</br>";
                if ( ! empty($newPoint) && $newPoint->isZero()) {

                    $this->points->setPointValueByHeightWidth($newY, $newX, 1);

                }
            }
        }

    }

    public function findNotZeroPoint() {
        for ($y = 0; $y < $this->height; $y++) {
            for ($x = 0; $x < $this->width; $x++) {
                $point = $this->points->getPointByHeightWidth($y, $x);


                if ( ! $point->isZero() && $point->flag == false) {
                    $this->points->setPointFlagByHeightWidth($y, $x, true);
//                    echo "findNotZeroPoint"; echo "<br>";
//                    var_dump($point);
//                    echo "<br>";
                    return $point;
                }
            }
        }
    }

    public function getMatrixSum() {
        $result = 0;
        for ($y = 0; $y < $this->height; $y++) {
            for ($x = 0; $x < $this->width; $x++) {
                $point = $this->points->getPointByHeightWidth($y, $x);
                if ( ! $point->isZero() ) {
                    $result++;
                }
            }
        }
        return $result;
    }

    public function fillMatrix()
    {
          for ($result = 0, $this->steps = 0; !($this->isEqualSum($result))  ; $this->steps++){
            $point = $this->findNotZeroPoint();
//            var_dump($point);

            $this->fillPointsAroundPointbyNumber($point);
//              var_dump($this->points);
            $result = $this->getMatrixSum();
//            var_dump($result);

        }
        return $this->steps;

    }
}
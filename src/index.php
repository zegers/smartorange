<?php
/**
 * Created by PhpStorm.
 * User: laninea
 * Date: 14.07.16
 * Time: 13:09
 */
require_once __DIR__.'/vendor/autoload.php';
require_once "connection.php";
require_once "task1/task1.php";
include 'task2/task2.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

$dataBase = connect($host, $dbName, $user, $password);

$app->error(function (\Exception $e, Request $request, $code) {
    switch ($code) {
        case 404:
            $message = '<h1>The requested page could not be found.</h1>';
            break;
        default:
            $message = '<h1>We are sorry, but something went terribly wrong.</h1>';
    }

    return new Response($message);
});

$app->get('/', function (Request $request) use ( $app) {

    $req = $request->getPathInfo();
    return $app["twig"]->render("main.twig", array('request' => $req));
})->bind('home');

$app->get('/show/films', function (Request $request) use ($app, $dataBase) {


    $films = selectFilms($dataBase, 1);
    $req = $request->getPathInfo();
    return $app["twig"]->render("films.twig", array("films" =>$films, 'request' => $req));
})->bind('showFilms');

$app->post('/add/film', function (Request $request) use ($app, $dataBase) {

    $isActive = false;
    if ($request->get('active') === 'on') {
        $isActive = true;
    }

    $dataArray = array('name' =>$request->get('name'),
                        'year' =>$request->get('year'),
                        'isActive' => $isActive);
    if (addFilm($dataBase, $dataArray))
    return $app->redirect('/');
});

$app->get('/add/film', function (Request $request) use ($app) {


    $req = $request->getPathInfo();

    return $app["twig"]->render("addFilm.twig", array('request' => $req));
})->bind('addFilm');

$app->get('/task/1', function (Request $request) use ($app, $arr) {
    $req = $request->getPathInfo();
    return $app["twig"]->render("task1.twig", array("arr" =>$arr, 'request' => $req));
})->bind('task1');

$app->get('/task/2', function (Request $request) use ($app, $steps, $matrixSize) {
    
   
    $req = $request->getPathInfo();
    return $app["twig"]->render("task2.twig", array('request' => $req, 'steps' => $steps, 'size' => $matrixSize));
})->bind('task2');


$app->run();


function connect($host, $dbName, $user, $password) {
    try {
        $db = new PDO("mysql:host=$host; dbname=$dbName", $user, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->exec("set names utf8");
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    return $db;
}

function selectFilms($db, $isActive = 1) {
    $sql = "SELECT * FROM Films WHERE isActive LIKE :isActive;";
    $q = $db->prepare($sql);
    $stmt = $q->execute(array(":isActive" => $isActive));
    $q->setFetchMode(PDO::FETCH_ASSOC);


    $films = array();
    while($row = $q->fetch(PDO::FETCH_ASSOC)) {
        array_push($films, $row);

    }
    return $films;
}
function addFilm($db, $data) {
    $sql = "INSERT INTO Films (NAME, YEAR, isActive) VALUES (:name, :year, :isActive);";
    $q = $db->prepare($sql);
    $q->bindValue(':name', $data['name']);
    $q->bindValue(':year', $data['year']);
    $q->bindValue('isActive', $data['isActive']);

    $stmt = $q->execute();
    if ($stmt) {
        return true;
    }
    return false;
}









